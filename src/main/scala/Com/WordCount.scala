package Com

import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object WordCount {
  def main(args: Array[String]) {
    var masterUrl = "local[1]"
    var inputPath = "D:\\Spark\\TestData\\Hamlet.txt"
//    var outputPath = "D:\\SparkExpression\\output1"
    var outputPath="hdfs://132.232.64.160:9000/wordCount.txt";

    if (args.length == 1) {
      masterUrl = args(0)
    } else if (args.length == 3) {
      masterUrl = args(0)
      inputPath = args(1)
      outputPath = args(2)
    }

    println(s"masterUrl:${masterUrl}, inputPath: ${inputPath}, outputPath: ${outputPath}")

    val sparkConf = new SparkConf().setMaster(masterUrl).setAppName("WordCount")
    val sc = new SparkContext(sparkConf)

    val rowRdd = sc.textFile(inputPath)
    rowRdd.persist(StorageLevel.MEMORY_ONLY)
    val resultRdd = rowRdd.flatMap(line => line.split(" "))
      .map(word => (word, 1)).reduceByKey(_ + _)

//    resultRdd.foreach(println)
    resultRdd.saveAsTextFile(outputPath)

    sc.stop();
  }
}
