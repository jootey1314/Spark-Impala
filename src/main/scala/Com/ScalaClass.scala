package Com

object ScalaClass {
  def main(args: Array[String]): Unit = {

    val jam = new Employee
    jam.salary = 80
    jam.name = "jame"
    println(jam.toString)
  }
}


class Person {
  var name = "";

  override def toString = getClass.getName + "[className=" + name + "]";

}


class Employee extends Person {
  var salary = 0.0;

  override def toString: String = super.toString + "[salary=" + salary + "]"

  printPoint()

  def printPoint(): Unit = {
    println("x 的坐标点 : " + 10)
    println("y 的坐标点 : " + 87)
  }
}

