import java.net.URL

import org.apache.spark.rdd.RDD
import org.apache.spark.{Partitioner, SparkConf, SparkContext}

import scala.collection.mutable

object CustomPartitionDemo {

  def main(args: Array[String]): Unit = {


    val path = "D:\\SparkExpression\\doc\\partition.txt"
    val conf = new SparkConf().setAppName("parition").setMaster("local[1]")
    val sc = new SparkContext(conf)


    val inputRdd = sc.textFile(path)
    val subjectAndTeacher: RDD[(String, String)] = GetSubjectAddTeacher(inputRdd).cache()


    val subjectList = List("bigdata", "java", "php")

    for (subject <- subjectList) {
      val bigdataSubject: RDD[(String, String)] = subjectAndTeacher.filter(_._1.equals(subject))
      val bigTake: Array[((String, String), Int)] = bigdataSubject.map((_, 1)).reduceByKey(_ + _).sortBy(_._2, false).take(2)
      println(bigTake.toBuffer)
    }

    sc.stop()
  }


  def GetSubjectAddTeacher(lines: RDD[String]) = {
    lines.map(line => {
      val url = new URL(line)
      val host = url.getHost
      val subject = host.substring(0, host.indexOf("."))
      val teacher = url.getPath.substring(1)
      (subject, teacher)
    })
  }


}


class SubjectPartitioner2(subjects: Array[String]) extends Partitioner {

  override def numPartitions: Int = subjects.length + 1

  var map = new mutable.HashMap[String, Int]()

  var i = 1

  for (sub <- subjects) {
    map += (sub -> i)
    i += 1
  }


  override def getPartition(key: Any): Int = {
    val k = key.toString
    map.getOrElse(k, 0)
  }
}


class SubjectPartitioner extends Partitioner {

  //    val subjectPartion = new SubjectPartitioner
  //    val tuplSubjectAndTeacherWithCount: RDD[((String, String), Int)] = subjectAndTeacher.map((_, 1)).reduceByKey(subjectPartion, _ + _)
  //    val tuples: Array[((String, String), Int)] = tuplSubjectAndTeacherWithCount.collect()
  override def numPartitions: Int = 2

  var pationsRules = Map("bigdata" -> 1, "java" -> 2)

  override def getPartition(key: Any): Int = {
    val k = key.toString
    pationsRules.getOrElse(k, 0)
  }
}