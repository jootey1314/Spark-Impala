package Com

import org.apache.spark.{SparkConf, SparkContext}

object SparkContextApp {
  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setAppName("WordCount").setMaster("local[1]");
    val sparkContext = new SparkContext(sparkConf);
    //todo 业务代码




    sparkContext.stop();
  }
}
