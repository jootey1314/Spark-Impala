package SparkProvider

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object SparkProvider {
  private val conf: SparkConf = new SparkConf().setAppName("sparkApp").setMaster("local[*]")

  def GetSparkContext(appName: String = "app", workerCount: Any = 2): SparkContext = {
    conf.setMaster("local[" + workerCount + "]")
    conf.setAppName(appName)
    val sparkContext = new SparkContext(conf)
    return sparkContext
  }

  def GetSparkSession(appName: String = "app", workerCount: Any = 2): SparkSession = {
    conf.setMaster("local[" + workerCount + "]")
    conf.setAppName(appName)
    return SparkSession.builder().config(conf).getOrCreate()
  }


  //arg:   "name,age,sex"
  def GetStructType(arg: String): StructType = {
    try {

      //    val schema = StructType(
      //      List(
      //        StructField("diaper_name", StringType, true),
      //        StructField("diaper_price", StringType, true),
      //        StructField("diaper_url", StringType, true)
      //      )
      //    )
      if (arg.isEmpty)
        return null
      val fields = arg.split(",").map(filedName => StructField(filedName, StringType, nullable = true))
      if (fields.isEmpty)
        return null
      return StructType(fields)
    }
    catch {
      case _: Exception => {
        return null
      }
    }
  }


}

//object end
