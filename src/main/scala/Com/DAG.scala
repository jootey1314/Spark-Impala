package Com

import org.apache.spark.{SparkConf, SparkContext}

object DAG {

  def main(args: Array[String]): Unit = {


    val masterUrl = "local[1]"
    val appName = "DAG"

    val sparkConf = new SparkConf().setMaster(masterUrl).setAppName(appName)
    val sc = new SparkContext(sparkConf)

    val rdd1 = sc.parallelize(List(("hello", 1), ("tom", 1), ("hello", 2), ("tom", 2)))
    val rdd2 = sc.parallelize(List(("hello", 3), ("tom", 3), ("hello", 4), ("tom", 4)))
    val rdd3 = rdd1.join(rdd2)
    rdd3.count()


    val rdd11 = rdd1.groupByKey()
    val rdd22 = rdd2.groupByKey()
    val rdd33 = rdd11.join(rdd22)
    rdd33.count()

  }
}
