package Com

import java.net.URL

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object CustomPartitionOne {

  def main(args: Array[String]): Unit = {

    val input = "D:\\SparkExpression\\doc\\partition.txt"

    val customPartitionOne = new CustomPartitionOne()

    customPartitionOne.handle(input)
    customPartitionOne.StopSc()
  }
}

class CustomPartitionOne {
  var sc: SparkContext = null

  def initSpark: SparkContext = {
    val conf = new SparkConf();
    conf.setAppName("Partition").setMaster("local[*]")
    sc = new SparkContext(conf)
    sc
  }

  def StopSc(): Unit = {
    sc.stop()
  }

  def handle(input: String) = {

    val sc = initSpark

    val lines: RDD[String] = sc.textFile(input)

    val subjectAndTeacher = SubjectAndTeacher(lines)

    val reduced = subjectAndTeacher.map((_, 1)).reduceByKey(_ + _)

    val group: RDD[(String, Iterable[((String, String), Int)])] = reduced.groupBy(_._1._1)

    val take: RDD[(String, List[((String, String), Int)])] = group.mapValues(_.toList.sortBy(_._2).reverse.take(2))


    val takeValue = take.values

    val result = takeValue.collect()

    println(result.toBuffer)
  }


  def SubjectAndTeacher(lines: RDD[String]) = {
    lines.map(line => {
      val url = new URL(line)
      val host = url.getHost()
      val subject = host.substring(0, host.indexOf("."))
      val teacher = url.getPath.substring(1)
      (subject, teacher)
    })
  }


}




