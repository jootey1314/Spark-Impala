//package Com
//
//import java.net.URL
//
//import org.apache.spark.rdd.RDD
//import org.apache.spark.{Partitioner, SparkConf, SparkContext}
//
//
//import scala.collection.mutable
//
//
//object CustomPartionTwoo {
//
//  def main(args: Array[String]): Unit = {
//    val path = "D:\\Spark\\TestData\\partition.txt"
//
//    val conf = new SparkConf().setAppName("spark").setMaster("local[1]")
//    val sc = new SparkContext(conf)
//
//    val textFile = sc.textFile(path)
//
//    val subjectAndTeacher = GetSubjectAndTeachers(textFile)
//    val reduced: RDD[((String, String), Int)] = subjectAndTeacher.map((_, 1)).reduceByKey(_ + _).cache()
//
//
//    val subjects = reduced.map(_._1._1).distinct().collect()
//    val subjectPartitioned = new SubjectPartitioner(subjects)
//
//
//    val reducedPartitions: RDD[(String, (String, Int))] = reduced.map(t => (t._1._1, (t._1._2, t._2))).partitionBy(subjectPartitioned)
//    val result: RDD[(String, (String, Int))] = reducedPartitions.mapPartitions(_.toList.sortBy(_._2._2).reverse.take(3).toIterator)
//
//    val res = result.collect()
//
//    println(res.toBuffer)
//
//
//
//    //    sc.stop()
//  }
//
//
//  def GetSubjectAndTeachers(source: RDD[String]) = {
//    source.map(line => {
//      val url = new URL(line)
//      val host = url.getHost
//      val subject = host.substring(0, host.indexOf("."))
//      val teacher = url.getPath.substring(1)
//      (subject, teacher)
//    })
//  }
//}
//
//
//class SubjectPartitioner(subjects: Array[String]) extends Partitioner {
//  //
//  //  var i = 1
//  //  var rules = new mutable.HashMap[String, Int]()
//  //  for (sub <- subjects) {
//  //    rules += (sub -> i)
//  //    i += 1
//  //  }
////  override def numPartitions: Int = subjects.length + 1
//
//  val rules = Map("bigdata" -> 1, "java" -> 2, "php" -> 3)
//
//  override def numPartitions: Int = rules.size + 1
//
//  override def getPartition(key: Any): Int = {
//    val k = key.toString
//    rules.getOrElse(k, 0)
//  }
//}