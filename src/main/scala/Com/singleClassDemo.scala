package Com

object singleClassDemo {

  def main(args: Array[String]): Unit = {
    println(Marker("red"))
    println(Marker.getMarker("blue"))
  }
}

//私有构造方法
class Marker private(val color: String) {
  println("创建" + this)

  override def toString: String = "颜色:" + color
}

//半生对象，可访问上述对象私有属性和方法
object Marker {

  private val markers: Map[String, Marker] = Map(
    "red" -> new Marker("red"),
    "blue" -> new Marker("blue"),
    "green" -> new Marker("green")
  )

  def apply(color: String) = {
    if (markers.contains(color)) markers(color) else null
  }


  def getMarker(color: String) = {
    apply(color)
  }

}

