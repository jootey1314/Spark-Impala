package Com

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}

object SparkSqlDemo {

  //不能和调用方，放在统一个类里面
  case class Person(name: String, sex: String, age: String, provice: String)

  def main(args: Array[String]): Unit = {


    val sparkConf = new SparkConf().setAppName("SparkSqlDemo").setMaster("local[1]");
    val sc = new SparkContext(sparkConf);
    //todo 业务代码
    val list = sc.parallelize(
      List(
        "joo,man,30,sc",
        "edward,man,30,sc",
        "sam,man,30,sc"
      ))


    val mapList: RDD[Person] = list.map(_.split(",")).map(m => Person(m(0), m(1), m(2), m(3)))


    val SqlContext = new SQLContext(sc)

    import SqlContext.implicits._

    val personDF = mapList.toDF

    //方式一
//    personDF.registerTempTable("person")
    personDF.createTempView("person")
    val sqlResult = SqlContext.sql("select * from person")

    //方式二DSL
//    val result=personDF.select("name","sex").where(personDF.col("name")>"jooper")

    sqlResult.show()

    //存储成外部json文件
//    sqlResult.write.json("hdfs://localhost:9000/sql3")

//    sc.stop()





  }
}