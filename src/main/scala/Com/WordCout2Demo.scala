package Com

import org.apache.spark.{SparkConf, SparkContext}

object WordCout2Demo {

  def main(args: Array[String]): Unit = {

    val sparkConf = new SparkConf()
//    sparkConf .setMaster("local[*]").setAppName("WordCount")
    System.setProperty("user.name", "hadoop")
    sparkConf.set("spark.app.name","wordCount");
    sparkConf.set("spark.master","local[*]");
    sparkConf.set("user.name","hadoop");

    val sc = new SparkContext(sparkConf)

    var inputPath = "D:\\log.txt"
//    inputPath="hdfs://192.168.200.24:8020/user/output/WordCount";
//    inputPath="hdfs://132.232.64.160:8020/testDatas/"

    val input = sc.textFile(inputPath)
    val filterVal = input.filter(x => x.size > 0)
    val lines = filterVal.map(_.split(" "))

    val wordKV = lines.map(w => (w(0), 1))
    val result = wordKV.reduceByKey(_ + _)
//    result.collect()

    result.foreach(println)

//    println(input.toDebugString)
  }
}
