package Com

import java.net.URL

import org.apache.spark.rdd.RDD
import org.apache.spark.{Partitioner, SparkConf, SparkContext}

import scala.collection.mutable

object CustomPartionTwo {

  def main(args: Array[String]): Unit = {
    val path = "D:\\SparkExpression\\doc\\partition.txt"
    val paritionHandle = new PartionHandle(path)
    paritionHandle.GetLastResult()

  }
}

//
//class SparkHelper {
//
//  //  var sc: SparkContext = null
//
//  def GetSc: SparkContext = {
//    val conf = new SparkConf()
//    conf.setAppName("Partition").setMaster("local[*]")
//    conf.set("spark.driver.allowMultipleContexts", "true")
//
//    val sc = new SparkContext(conf)
//    sc
//  }
//
//  //  def StopSc() = {
//  //    sc.stop()
//  //  }
//
//}

class PartionHandle(path: String) {

  def GetDataSource: RDD[String] = {

    //    val sc = new SparkHelper().GetSc


    val conf = new SparkConf().setAppName("Partition").setMaster("local[*]")
    conf.set("spark.driver.allowMultipleContexts", "true")
    val sc = new SparkContext(conf)
    val textSource = sc.textFile(path)
    textSource
  }


  def GetSubjects = {
    val reduced: RDD[((String, String), Int)] = GetReduced
    val subjects = reduced.map(_._1._1).distinct().collect()
    (subjects, reduced)
  }


  private def GetReduced: RDD[((String, String), Int)] = {
    val source = GetDataSource
    val subjectAndTeacher = GetSubjectAndTeachers(source)
    val reduced: RDD[((String, String), Int)] = subjectAndTeacher.map((_, 1)).reduceByKey(_ + _)
    reduced
  }

  def GetSubjectAndTeachers(source: RDD[String]) = {
    source.map(line => {
      val url = new URL(line)
      val host = url.getHost
      val subject = host.substring(0, host.indexOf("."))
      val teacher = url.getPath.substring(1)
      (subject, teacher)
    })
  }


  def GetLastResult() = {

    val subjects = GetSubjects

    val subjectPartitioned = new SubjectPartitioner(subjects._1)

    val reduced: RDD[((String, String), Int)] = subjects._2

    val mapValue = reduced.map(x => (x._1._1, (x._1._2, x._2)))


    val reducedPartitions: RDD[(String, (String, Int))] = mapValue.partitionBy(subjectPartitioned)

//    val result: RDD[(String, (String, Int))] = reducedPartitions.mapPartitions(_.toList.sortBy(_._2._2).reverse.take(2).toIterator)

    val result: Array[(String, (String, Int))] = reducedPartitions.collect()

//    val res = result.collect()

//    println(res.toBuffer)

    //    new SparkHelper().StopSc()
  }

}


class SubjectPartitioner(subjects: Array[String]) extends Partitioner {
  override def numPartitions: Int = subjects.length

  var i = 1
  var rules = new mutable.HashMap[String, Int]()
  for (sub <- subjects) {
    rules += (sub -> i)
    i += 1
  }

  override def getPartition(key: Any): Int = {
    val k = key.toString
    rules.getOrElse(k, 0)
  }
}