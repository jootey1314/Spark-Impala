package Com

import org.apache.spark.{SparkConf, SparkContext}

object RddCheckPoint {
  def main(args: Array[String]): Unit = {

    var masterUrl = "local[1]"
    var inputPath = "D:\\SparkExpression\\data\\Hamlet.txt"
    var outputPath = "D:\\SparkExpression\\output1"
    val checkPointPath = "hdfs://localhost:9000/checkPoint100";

    if (args.length == 1) {
      masterUrl = args(0)
    } else if (args.length == 3) {
      masterUrl = args(0)
      inputPath = args(1)
      outputPath = args(2)
    }

    println(s"masterUrl:${masterUrl}, inputPath: ${inputPath}, outputPath: ${outputPath}")

    val sparkConf = new SparkConf().setMaster(masterUrl).setAppName("WordCount")
    val sc = new SparkContext(sparkConf)


    sc.setCheckpointDir(checkPointPath);
    val rdd1 = sc.textFile(inputPath).filter(_.contains("scala"));
    rdd1.cache();
    rdd1.checkpoint();
    rdd1.count()


  }


}
