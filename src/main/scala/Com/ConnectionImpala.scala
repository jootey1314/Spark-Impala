package Com

import java.sql.{DriverManager, SQLException, Statement}

import org.apache.spark.{SparkConf, SparkContext}

class ConnectionImpala {

  def CreateTable(): Unit = {
    val Context = GetContext;
    try {
//      var sqlCreateTable =
//        """
//        drop table if exists phone
//        create external table phone
//        (
//        brand string,
//        price string,
//        fee string,
//        sales string,
//        pl string,
//        city string,
//        nick string,
//        img string
//        )
//        row format delimited fields terminated by ' \\t'
//        location '/impala/'
//        """;

//      Context.execute(sqlCreateTable)

//      return

    var  sqlCreateTable="drop table if exists phone1"
      Context.execute(sqlCreateTable)

      sqlCreateTable="create external table phone1 ( brand string, price string,fee string,sales string,pl string,city string, nick string, img string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t'  LOCATION  '/impala/'";
      Context.execute(sqlCreateTable)

//      sqlCreateTable="load data inpath '/impala/sjtb1.txt' into table phone1"  先建立表在此导入数据
//      Context.execute(sqlCreateTable);
    } finally {
      Context.close();
    }
  }

  def GetDataFromImpala(): Unit = {
    val stmt: Statement = GetContext
    try {
      var query: String = "select * from sjtb"
      val rs = stmt.executeQuery(query)

      while (rs.next()) {
        System.out.println(rs.getString("pp") + ":");
      }

    } catch {
      case e: SQLException => e.printStackTrace()
    } finally {
      stmt.close()
    }
    //
    //    val resultSetList = Iterator.continually((rs.next(), rs)).takeWhile(_._1).map(r => {
    //      //    getRowFromResultSet(r._2)
    ////      (ResultSet) => (spark.sql.Row)
    //    }).toList
    //
    //    sc.parallelize(resultSetList)
  }

  private def GetContext = {
    val JDBCDriver = "com.cloudera.impala.jdbc41.Driver"
    val ConnectionURL = "jdbc:impala://192.168.200.24:21050/default;auth=noSasl"
    Class.forName(JDBCDriver).newInstance
    val con = DriverManager.getConnection(ConnectionURL)
    val stmt = con.createStatement()
    stmt
  }

  def InsertData(uid: String): Unit = {
    val stmt: Statement = GetContext
    try {
      val sql = "insert into person(uid) values('" + uid + "')"

      stmt.execute(sql)
    } catch {
      case e: SQLException =>
        e.printStackTrace()
    } finally {
      stmt.close()
    }
  }

}

object ConnectionImpala {
  def main(args: Array[String]): Unit = {
    val obj = new ConnectionImpala();
    obj.CreateTable();
    //    obj.InsertData("xuanxuan");
//    obj.GetDataFromImpala();
  }
}

//
//import java.sql.DriverManager
//import java.sql.ResultSet
//import java.sql.SQLException
//
//object ImpalaTestCase { // here is an example query based on one of the Hue Beeswax sample tables
//  private val SQL_STATEMENT = "SELECT * FROM person"
//  // set the impalad host
//  private val IMPALAD_HOST = "192.168.200.24"
//  // port 21050 is the default impalad JDBC port
//  private val IMPALAD_JDBC_PORT = "21050"
//  private val CONNECTION_URL = "jdbc:impala://" + IMPALAD_HOST + ':' + IMPALAD_JDBC_PORT + "/default;auth=noSasl"
//  //    val ConnectionURL = "jdbc:impala://192.168.200.24:21050/default;auth=noSasl"
//  private val JDBC_DRIVER_NAME = "com.cloudera.impala.jdbc41.Driver"
//
//  def main(args: Array[String]): Unit = {
//    System.out.println("\n=============================================")
//    System.out.println("Cloudera Impala JDBC Example")
//    System.out.println("Using Connection URL: " + CONNECTION_URL)
//    System.out.println("Running Query: " + SQL_STATEMENT)
//    var con = null
//    try {
//      Class.forName(JDBC_DRIVER_NAME)
//      con = DriverManager.getConnection(CONNECTION_URL)
//      val stmt = con.createStatement
//      //insert(stmt);
//      //delete(stmt);
//      val rs = stmt.executeQuery(SQL_STATEMENT)
//      System.out.println("\n== Begin Query Results ======================")
//      // print the results to the console
//      while ( {
//        rs.next
//      }) { // the example query returns one String column
//        System.out.print(rs.getString("userid") + ":")
//        System.out.println(rs.getString("age"))
//      }
//      System.out.println("== End Query Results =======================\n\n")
//    } catch {
//      case e: SQLException =>
//        e.printStackTrace()
//      case e: Exception =>
//        e.printStackTrace()
//    } finally try
//      con.close
//    catch {
//      case e: Exception =>
//
//      // swallow
//    }
//  }
//
//  def insert(stmt: Nothing): Unit = {
//    val sql = " INSERT INTO tab001(userid, age, city,name) VALUES (101, 22, 'bj','wanghongxiang001') "
//    try
//      stmt.execute(sql)
//    catch {
//      case e: SQLException =>
//        // TODO Auto-generated catch block
//        e.printStackTrace()
//    }
//  }
//}
