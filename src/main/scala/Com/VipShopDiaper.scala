package Com

import SparkProvider.SparkProvider
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext, SparkSession}

class VipShopDiaper {

  val path = "F:\\Expression\\spark\\VipShopDiaper(1).json"

  //不能和调用方，放在统一个类里面
  case class Person(diaper_name: String, diaper_price: String, diaper_url: String)

  def ReadJson(): Any = {

    val sparkSession = SparkProvider.GetSparkSession("vipshop", "*")

    val rdd: RDD[String] = sparkSession.sparkContext.textFile(path)
    val dataFrame = sparkSession.read.json(rdd).cache()

    dataFrame.createTempView("vip")

    val sql =
      """
        |select
        | diaper_name,
        | diaper_price,
        | diaper_url
        | from
        | vip""".stripMargin

    sparkSession.implicits;
    implicit val mapEncoder = org.apache.spark.sql.Encoders.kryo[Map[String, Any]]
    val maps: Array[Map[String, Any]] = sparkSession.sql(sql).map(teenager => teenager.getValuesMap[Any](List("diaper_name", "diaper_price", "diaper_url"))).collect() //.as[Person].show()
    val result = maps.map(m => (m("diaper_price").toString, m("diaper_name"), m("diaper_url"))).sortBy(x => (x._1, false))

    result.foreach(println)

  }

  def bk(): Unit = {
    val scConf = new SparkConf().setMaster("local[2]").setAppName("vipShop")
    val sc = new SparkContext(scConf)
    //    val jsonStr = sc.textFile("F:\\Expression\\spark\\VipShopDiaper.json");
    //    val result = jsonStr.map(s => JSON.parseFull(s));//逐个JSON字符串解析//
    //    result.foreach(println)


    val sqlSc = new SQLContext(sc)

    //    val df: DataFrame = sqlSc.read.json("F:\\Expression\\spark\\VipShopDiaper.json").cache()

    //    val df = sqlSc.read.format("json").load(path)

    val df = sqlSc.read.json(path).cache()
    df.filter(df("_corrupt_record").isNotNull).count()

    // 因为有点多只显示1条，不截断
    //    df.show(1,false)
    println(df.printSchema())
    df.select("_corrupt_record").show(1, false)
  }

}

object VipShopDiaper {
  def main(args: Array[String]): Unit = {
    val sparkSessionOp = new VipShopDiaper
    sparkSessionOp.ReadJson
  }
}
