package Com

import org.apache.spark.sql.{Dataset, SparkSession}

//demo，无法成功执行
object SparkUDFDemo {

  def avg = () => {};

  def main(args: Array[String]): Unit = {

    val sparkSession = SparkSession.builder().appName("sqllSession").master("local[*]").getOrCreate()

    val fileDs: Dataset[String] = sparkSession.read.textFile("hdfs://192.168.200.24:8020/")

    fileDs.toDF().createTempView("v_user")

    sparkSession.udf.register("avg", avg);

    sparkSession.sql("select avg(id) from v_user");
  }
}
